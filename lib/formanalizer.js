var _hasOwnProperty = function (obj, prop) {
  return Object.prototype.hasOwnProperty.call(obj, prop);
};

/**
 *  Grabs all form's elements and collects in the returned object.
 *  @param {DOM element} form
 *  @return {Object}
 */
var grabForm = exports.grabForm = function (form) {
  if (!form || !form.elements || form.elements.length === 0) {
    return {};
  }

  var result = {};
  var elements = form.elements;
  for (var i = 0, len = elements.length; i < len; i += 1) {
    var element = elements[i];
    var name = element.name;
    var value;

    if (element.type === 'radio' || element.type === 'checkbox') {
      value = element.checked ? element.value : 'off';
    } else {
      value = element.value;
    }

    if (value === 'off') {
      continue;
    }

    if (name && name.length > 0) {
      if (typeof result[name] === 'undefined') {
        result[name] = value;
      } else if (result[name] instanceof Array) {
        result[name].push(value);	
      } else {
        result[name] = [result[name], value];
      }
    }
  }
  return result;
};

var compareObjects = exports.compareObjects = function (obj1, obj2) {
  var result = {}, value = '', elname;

  if (JSON.stringify(obj1) === JSON.stringify(obj2)) {
    return null;
  }

  for (elname in obj2) {

    if(!_hasOwnProperty(obj2, elname)) {
      continue;
    }

    value = obj2[elname];
    if (typeof obj1[elname] === 'undefined' || 
        typeof obj1[elname] !== typeof obj2[elname] ||
          obj1[elname] !== obj2[elname]) {
      if (obj1[elname] instanceof Array && obj2[elname] instanceof Array) {
        if(JSON.stringify(obj1[elname]) !== JSON.stringify(obj2[elname])) {
          result[elname] = obj2[elname];
        }
      } else {
        result[elname] = obj2[elname];
      }
    }
  }

  for (elname in obj1) {
    if (!_hasOwnProperty(obj1, elname)) {
      continue;
    }
    if (typeof obj2[elname] === 'undefined') {
      value = obj1[elname];
      switch (value) {
        case 'on':
          result[elname] = 'off';
          break;
        case 'off':
          result[elname] = 'on';
          break;
        default:
          result[elname] = '';
      }
    }
  }

  return result;
};
