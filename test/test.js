var should = require('chai').should();
var fa = require('../lib/formanalizer.js');
var compareObjects = fa.compareObjects;

describe('formanalizer', function () {

  describe('compareObjects', function() {
    var obj1 = {
      name: 'Denis',
      age: '31',
      skills: ['javascript', 'node', 'mocha']
    };
    var obj2 = {
      name: 'Denis',
      age: '22',
      programmer: 'on',
      skills: ['html']
    };
    var obj3 = {
      name: 'Denis',
      age: '22',
      skills: ['html']
    };

    it('should return null', function () {
      var res = compareObjects(obj1, obj1);
      should.equal(res, null);
    });

    it('should return difference 1', function () {
      var res = compareObjects(obj1, obj2);
      JSON.stringify(res).should.equal('{"age":"22","programmer":"on","skills":["html"]}');
    });
    
    it('should return difference 2', function () {
      var res = compareObjects(obj2, obj3);
      JSON.stringify(res).should.equal('{"programmer":"off"}');
    });

  });
});
