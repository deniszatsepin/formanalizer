module.exports = function(grunt) {

  grunt.initConfig({
    jshint: {
      options: {
        curly: true,
        eqeqeq: true,
        eqnull: true,
        laxcomma: true,
        node: true,
        devel: true,
        globals: {
          jQuery: true
        },
      },

      all: ['Gruntfile.js', 'lib/**/*.js', 'public/js/*.js']
    },
    // Configure a mochaTest task
    mochaTest: {
      test: {
        options: {
          reporter: 'spec'
        },
        src: ['test/**/*.js']
      }
    },
    copy: {
      main: {
        files: [
          { expand: true, cwd: 'public/', src: '*.html', dest: 'build/' },
          { expand: true, cwd: 'public/', src: 'css/*.css', dest: 'build/' }
        ]
      }
    },
    browserify: {
      dist: {
        files: {
          'build/js/bundle.js': ['public/js/**/*.js'],
        }
      }
    }
  });

  // Add the tasks here.
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-mocha-test');
  grunt.loadNpmTasks('grunt-browserify');
  grunt.loadNpmTasks('grunt-contrib-copy');

  grunt.registerTask('hint', 'jshint');
  grunt.registerTask('test', ['mochaTest']);

  grunt.registerTask('default', ['hint', 'test', 'copy', 'browserify']);
};
