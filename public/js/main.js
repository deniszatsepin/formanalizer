var fa = require('../../lib/formanalizer');

var form = document.getElementById('form');
var results = document.getElementById('results');

var slice = Array.prototype.slice;
var proxy = function(fn, ctx) {
  var args = slice.call(arguments, 2);
  return function() {
    var arg = slice.call(arguments).concat(args);
    fn.apply(ctx, arg);
  };
};

var FormChecker = (function(){
  var FormChecker = function(){
    this.init.apply(this, arguments);
  };

  FormChecker.prototype.init = function (form, results) {
    this.form = form;
    this.results = results;
    this.current = fa.grabForm(this);

    form.addEventListener('submit', proxy(function(e){
      e.preventDefault();
      this.check();
    }, this), false); 
  };

  FormChecker.prototype.check = function () {
    var cur = fa.grabForm(this.form);
    this.diff = fa.compareObjects(this.current, cur); 
    this.current = cur;
    this.results.innerHTML = 'Для отправки на сервер: ' + JSON.stringify(this.diff);
  };

  return FormChecker;
})();

if (form && results) {
  var formChacker = new FormChecker(form, results);


}
